
var OverAchiever = function(obj) {

	this.initRan = false;
	//these need to be set initially
	this.root = "/";
	this.imgFolder = 'img';
	this.audioFolder = 'audio';
	this.filename = "achievement"
	this.playAudio = false;
	this.useDebug = false;
	this.useLocal = false;   //use local storage to keep the achievement data current
	this.boxstring = '<div class="achievement-box oa-hide"><div class="oa-col1"><img src="http://placehold.it/50x50"/></div><div class="oa-col2"><p class="oa-title">TITLE</p><p class="oa-desc">DESC</p></div><div class="clear"></div></div>"';
	this.allAchievements = null;

	//OVERRIDE SET DEFAULTS
	if(obj !== null)
	{
		for (var prop in obj) 
			if (obj.hasOwnProperty(prop)) 
				this[prop] = obj[prop];
	}

	this.filename += ".json";
	this.achievementBox = $(this.boxstring);

	this.loadData();

	//create custom events for external use
	this.event = new this.EventManager(this);
	/*
	add an addeventonframe event
	//custom events list
	//acheckupdate: fires whenever a oa has checked for updates
	*/

};

OverAchiever.prototype.init =function(){

	this.clog("init ran");

	this.clog(this.allAchievements);

	//check for acheivement events
	var tthis = this;
	/*
	this.checkInt = setInterval(function(){
		tthis.event.fire("acheckupdate", {somdata:5*5});
	}, 100);*/

	$('body').append(this.achievementBox);

};

OverAchiever.prototype.loadData = function(){
	var ot = this;
	$.getJSON(this.root + "/" + this.filename, function(data){
		ot.clog("data loaded.");
		ot.allAchievements = data.achievements;

		//check for local storage and set data according to that
		if(ot.useLocal)
			ot.loadLocal();
		ot.init();
	});
};

/**
* Update the info on achievement won
*/
OverAchiever.prototype.winAchievementByName = function(name){

	for(var i=0; i < this.allAchievements.length; i++)
	{
		var a = this.allAchievements[i];
		if(a.title === name && !a.won)
		{
			if(this.playAudio)
			{
				var afName = this.root+'/'+this.audioFolder+'/'+a.audio;
				this.clog(afName);
				var aud = new Audio(afName);
				console.log(aud);
				aud.play();
			}
			//set the achievenment box values
			//local path
			var imgPath = this.root+'/'+this.imgFolder+'/'+ a.img;
			//abs path online
			if(a.img.indexOf('http') != -1)
				imgPath = a.img;
			$(this.achievementBox.find('img')).attr("src", imgPath);
			$(this.achievementBox.find('.oa-title')).html(a.title);
			$(this.achievementBox.find('.oa-desc')).html(a.description);
			this.openItem(this.achievementBox);
			this.clog("got achiement: " + name + " !!!");
			this.allAchievements[i].won = true;

			//save data
			this.saveProgress();

			return;
		}
		else if(a.won)
			this.clog("You already have this achievement");
	}
	
};


/**
* Update the progress on specific achievement requirements
* name: achievementName
* obj: an json object with the requirement name and current
*      value to compare to the req needed to make to win
*/

OverAchiever.prototype.updateAchievementProgess = function(name, obj){
	for(var i=0; i < this.allAchievements.length; i++)
	{
		var a = this.allAchievements[i];
		if(a.title === name && !a.won)
		{
			var req = a.requirements
			for(var j=0; j < req.length; j++)
			{
				var r = req[j];
				this.clog(r);

				//compare list objects
				//iter over incoming objects
				for (var prop in obj)
				{
					//iter over existing achievement req objs
					for(var p in r)
					{
						if(prop === p)
						{
							this.clog("p is " + p + " and prop is " + prop);
							this.clog("pv is " + r[prop] + " and propv is " + obj[prop]);
							//test the values
							if(obj[prop] === r[prop])
							{
								this.clog("MATCH!");
								this.winAchievementByName(name);
								return;
							}
						}
					}//end for
					
				}//end for
			}
			return;
			
		}
		else if(a.won)
		{
			this.clog("You already have this achievement");
		}
	}
}

/*
* load all the achievements into a single list
*/
OverAchiever.prototype.showAllAchievements = function(){
	var rString = "";

	this.loadLocal();
	var i=0;
	for(i=0; i < this.allAchievements.length; i++)
	{
		var a = this.allAchievements[i];
		var bs = $(this.boxstring);

		var imgPath = this.root+'/'+this.imgFolder+'/'+ a.img;
		//abs path online
		if(a.img.indexOf('http') != -1)
			imgPath = a.img;
		//set the achievenment box values
		$(bs.find('img')).attr("src", imgPath);
		$(bs.find('.oa-title')).html(a.title);
		$(bs.find('.oa-desc')).html(a.description);
		bs.removeClass('oa-hide').addClass("oa-static");
		//check if won or not
		if(!a.won)
			bs.addClass('oa-unwon');

		rString += $('<div>').append(bs.clone()).html();
	}

	return rString;
}

/*
* Read local storage and set data based on that
*/
OverAchiever.prototype.loadLocal = function(){
	console.log("local storage load ran");
	if(this.canStore && localStorage.overachievements != undefined)
	{
		//
		console.log("local storage loaded: ");
		console.log(localStorage);
		var localData = $.parseJSON(localStorage['overachievements']);
		console.log(localData);
		this.allAchievements = localData;
	}
	else{return null;}
};

/*
*  save the achievement progress data on the local browser
*/
OverAchiever.prototype.saveProgress = function(){
	if(this.canStore)
	{
		//
		localStorage.overachievements = JSON.stringify(this.allAchievements);
	}
};

/*
*  clear the locally stored achievement progress
*/
OverAchiever.prototype.purge = function(){
	localStorage.clear();
};

OverAchiever.prototype.closeItem = function(itm)
{
	var it = itm[0];
	if(it.className.indexOf("oa-hide") == -1)
		it.className = it.className + " oa-hide";
};

OverAchiever.prototype.openItem = function(itm)
{
	var it = itm[0];
	it.className = it.className.replace(" oa-hide", "");
	//start countdown to hide item
	var tthis = this;
	var sto = setTimeout(function(){tthis.closeItem($(it));}, 3000);
};



/**=== UTILS ====**/

OverAchiever.prototype.EventManager = function(target) {
    var target = target || window,
        events = {};
    this.observe = function(eventName, cb) {
        if (events[eventName]) events[eventName].push(cb);
        else events[eventName] = new Array(cb);
        return target;
    };
    this.stopObserving = function(eventName, cb) {
        if (events[eventName]) {
            var i = events[eventName].indexOf(cb);
            if (i > -1) events[eventName].splice(i, 1);
            else return false;
            return true;
        }
        else return false;
    };
    this.fire = function(eventName) {
        if (!events[eventName]) return false;
        for (var i = 0; i < events[eventName].length; i++) {
            events[eventName][i].apply(target, Array.prototype.slice.call(arguments, 1));
        }
    };
};

/**
 * Returns nothing, used as switch to turn logging on/off
 *
 * Example: test what plaftform is being used
 * Utils.debuglog("hello world!");
 *
 */
OverAchiever.prototype.clog = function(s)
{
	if(this.useDebug)
		console.log(s);
};

OverAchiever.prototype.isMobile = function(){
	return navigator.userAgent.toLowerCase().match(/android|webos|iphone|ipad|ipod|blackberry/);
};

OverAchiever.prototype.isFirefox = function(){
	return (/Firefox/i.test(navigator.userAgent));
};

OverAchiever.prototype.canStore = function() {
	try { return 'localStorage' in window && window['localStorage'] !== null;} catch (e) { return false; }
}; 
	  


