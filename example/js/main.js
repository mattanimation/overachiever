$(document).ready(function(){

	var oa = new OverAchiever({
		root:$('body').attr('data-root'),
		filename: "achievements",
		playAudio: true,
		useDebug: false,
		useLocal: true
	});
	//oa.purge();

	oa.event.observe("acheckupdate", function(e){
		//console.log("getting update");
		//console.log(e);
		//console.log("data "+ e.someData);
	});


	$('#ach1').click(function(evt){
		oa.winAchievementByName("Button Click");
	});

	var totalKills = 0;
	$('#ach2').click(function(evt){
		totalKills++;
		//send update to "murderer" achievement which needs 10 kills total
		$('body').attr("style", "background-color: red;");
		oa.updateAchievementProgess("Murderer", {"kills":totalKills});
		console.log(totalKills);
		setTimeout(function(){$('body').attr("style", "background-color: white;");}, 100);
	});

	$('#seeAch').click(function(){
		$('.container').append($(oa.showAllAchievements()));
	});


});

