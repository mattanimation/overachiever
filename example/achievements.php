<?php
/*
* Template Name: Achievements
*/
?>

<?php get_template_part('head');?>
    <body data-root="<?php bloginfo('template_url');?>">

        <?php get_header();?>
        <section id="home">
            <div class="content-width grid">
                <div class="col_12">
                    <div class="top-offset"></div>
                    <h1 class="main-title">ACHIEVEMENTS</h1>
                    <div class="achievements-container">
                    </div>
                </div>
               
            </div>
        </section>



        <?php get_footer();?>
        <?php get_template_part('scripts');?>

    </body>
</html>
