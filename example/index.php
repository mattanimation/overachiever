<?php include 'head.php';?>

<?php
    function curPageURL() {
     $pageURL = 'http';
     if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
     $pageURL .= "://";
     if ($_SERVER["SERVER_PORT"] != "80") {
      $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
     } else {
      $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
     }
     return $pageURL;
    }
?>

    <body data-root="<?php echo curPageURL();?>">
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <?php include 'header.php';?>

        <!-- Add your site or application content here -->
        <div class="container">

            <h1> Over Achiever </h1>
            
            <button class="green" id="ach1">Click me</button>
            </hr>
            <button id="seeAch">see achievements</button>
            <button id="ach2">kill enemy</button>

        </div>

        <?php include 'footer.php';?>
        <?php include 'scripts.php';?>
    </body>
</html>
