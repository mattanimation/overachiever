# OverAchiever - (still work in progress, but functional)
## A simple javascript library to enable a site with an achievement system.

### see example here: http://www.atmosinteractive.com

***

* Step 1. create your achievements in the JSON file
* Step 2. include the overachiever css and js in your page
* Step 3. create and instance of the prototype

## Example Usage:
```
var oa = new OverAchiever({
    root:$('body').attr('data-root'),
    filename: "achievements",
    playAudio: true,
    useDebug: false
});
```
>root: the root path to your files.
>
>filename: the name of the json file that holds your achievements.
>
>playAudio: if set to true this will play the audio file defined in the json file, placed in /audio dir.
>
>useDebug: for development purposes.

***
### More examples:

You can can update your achievement progress
```
oa.updateAchievementProgess("Murderer", {"kills":totalKills});
```
kills is the required property and you send the current value to check against. If the value matches what is in the JSON file the achievement is set to "won" automagically.

How to set and achievement as "won"
```
oa.winAchievementByName("Name of Achievement");
```

